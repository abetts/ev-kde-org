---
title: "KDE e.V. Members"
layout: page
menu_active: Organization
---

If you are an active member of the KDE community and would like to join the KDE
e.V. please have a look at the <a href="/getinvolved/members/">information
how to become a member of the KDE e.V.</a>

These are the current active members (alphabetical by first name):

<noscript><p>This list only works if JavaScript is enabled.</p></noscript>

<ul id="memberList">
</ul>

<br style="clear:left;" />

<script>
fetch('https://my.kde.org/ev-members')
  .then(function(response) {
    return response.json();
  })
  .then(function(members) {
    /* These letters are sufficient for our first-name-sorting */ 
    const uniderp = (s) =>  s.toLowerCase().replace(/å/g,'a').replace(/à/g,'a').replace(/é/g,'e');
    memberNames = members["members"];
    memberNames.sort((a, b) => uniderp(a).localeCompare(uniderp(b)));
    
    const memberList = document.getElementById('memberList');
    memberNames.forEach(function(member) {
      const li = document.createElement('li');
      li.appendChild(document.createTextNode(member));
      memberList.appendChild(li);
    });
  });
</script>
