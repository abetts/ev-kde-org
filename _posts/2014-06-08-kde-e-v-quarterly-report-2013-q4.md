---
title: 'KDE e.V. Quarterly Report 2013 Q4'
date: 2014-06-08 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2013_Q4.pdf">fourth quarterly report of 2013</a>.

This report covers October through December 2013, including statements from the board, reports from sprints and a feature about KDE in Google Summer of Code and Google Code In.
      
