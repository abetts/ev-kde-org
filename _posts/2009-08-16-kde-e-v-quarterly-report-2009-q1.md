---
title: 'KDE e.V. Quarterly Report 2009 Q1'
date: 2009-08-16 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2009Q1.pdf">KDE e.V.
Quarterly Report</a> is now available for the first quarter of 2009, from January to March. This document includes reports of the board and the working groups about KDE e.V. activities at the beginning of 2009.
      
