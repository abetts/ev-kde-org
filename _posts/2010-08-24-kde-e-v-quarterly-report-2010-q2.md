---
title: 'KDE e.V. Quarterly Report 2010 Q2'
date: 2010-08-24 00:00:00 
layout: post
---

<a href="http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf">KDE e.V.
      Quarterly Report</a> is now available for the second quarter of 2010, from April to June. This document includes reports of the board and the working groups about KDE e.V. activities, reports from sprints, financial information, and more.
      